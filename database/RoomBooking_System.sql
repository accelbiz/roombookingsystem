/*
SQLyog Community v9.63 
MySQL - 5.5.28 : Database - RoomBookingSystem
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`RoomBookingSystem` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `RoomBookingSystem`;

/*Table structure for table `booking` */

DROP TABLE IF EXISTS `booking`;

CREATE TABLE `booking` (
  `booking_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `room_id` int(11) DEFAULT NULL,
  `time_slot_id` int(11) DEFAULT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `starttime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `endtime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`booking_id`),
  KEY `fk_BookedAudience_User_Id_idx` (`user_id`),
  KEY `fk_BookedAudience_Audience_idx` (`room_id`),
  CONSTRAINT `fk_Booking_Audience` FOREIGN KEY (`room_id`) REFERENCES `room` (`room_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Booking_User_Id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `booking` */

LOCK TABLES `booking` WRITE;

insert  into `booking`(`booking_id`,`user_id`,`room_id`,`time_slot_id`,`date`,`starttime`,`endtime`) values (1,1,1,0,'2017-01-20 00:00:00','2017-01-20 00:00:00','2017-01-20 00:00:00'),(2,1,3,0,'2017-01-20 22:30:00','2017-01-20 23:30:00','2017-01-20 22:30:00');

UNLOCK TABLES;

/*Table structure for table `room` */

DROP TABLE IF EXISTS `room`;

CREATE TABLE `room` (
  `room_id` int(11) NOT NULL AUTO_INCREMENT,
  `room_name` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `floor` int(11) NOT NULL,
  `places` int(11) NOT NULL,
  `room_type_id` int(11) NOT NULL,
  PRIMARY KEY (`room_id`),
  UNIQUE KEY `audience_name_UNIQUE` (`room_name`),
  KEY `fk_Audience_type_idx` (`room_type_id`),
  CONSTRAINT `fk_Room_type` FOREIGN KEY (`room_type_id`) REFERENCES `room_type` (`room_type_id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `room` */

LOCK TABLES `room` WRITE;

insert  into `room`(`room_id`,`room_name`,`floor`,`places`,`room_type_id`) values (1,'Apple',3,50,3),(2,'Mango',1,20,1),(3,'Orange',1,140,2),(4,'Banana',2,140,2),(5,'Potato',1,25,3),(6,'Onion',1,25,3),(7,'War Room',3,25,1),(8,'Guest',4,25,1),(9,'Maple',3,40,3),(10,'Caladium',3,40,3);

UNLOCK TABLES;

/*Table structure for table `room_type` */

DROP TABLE IF EXISTS `room_type`;

CREATE TABLE `room_type` (
  `room_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `room_type` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`room_type_id`),
  UNIQUE KEY `audience_type_UNIQUE` (`room_type`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `room_type` */

LOCK TABLES `room_type` WRITE;

insert  into `room_type`(`room_type_id`,`room_type`) values (2,'Auditorium'),(3,'Meeting Room'),(1,'VC');

UNLOCK TABLES;

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `first_name` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `designation` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `role` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email_UNIQUE` (`email`),
  KEY `fk_User_post_idx` (`designation`),
  KEY `fk_User_Role_idx` (`role`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `user` */

LOCK TABLES `user` WRITE;

insert  into `user`(`id`,`password`,`email`,`first_name`,`last_name`,`designation`,`role`) values (1,'password','test@testmail.com','Salamaan','Khan','SSE','Admin'),(3,'password','srk@testmail.com',NULL,'Khan','SSE','Admin');

UNLOCK TABLES;

/*Table structure for table `user_designation` */

DROP TABLE IF EXISTS `user_designation`;

CREATE TABLE `user_designation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `designation` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `title_UNIQUE` (`designation`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `user_designation` */

LOCK TABLES `user_designation` WRITE;

UNLOCK TABLES;

/*Table structure for table `user_role` */

DROP TABLE IF EXISTS `user_role`;

CREATE TABLE `user_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role` varchar(40) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `user_role` */

LOCK TABLES `user_role` WRITE;

UNLOCK TABLES;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
