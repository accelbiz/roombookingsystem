package com.impetus.roombookingsystem.microservices;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.impetus.roombookingsystem.microservices.EdgeServerApplication;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = EdgeServerApplication.class)
public class EdgeServerApplicationTests {

	@Test
	public void contextLoads() {
	}

}
