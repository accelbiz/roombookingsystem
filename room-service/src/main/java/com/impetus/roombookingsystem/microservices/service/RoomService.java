package com.impetus.roombookingsystem.microservices.service;

import java.util.List;

import com.impetus.roombookingsystem.microservices.domain.RoomDomain;

public interface RoomService {

	RoomDomain findByRoomId(Integer roomId);
	
	RoomDomain findByRoomName(String roomName);

	List<RoomDomain> getAllRooms();

	RoomDomain addNewRoom(RoomDomain room) throws Exception;

}