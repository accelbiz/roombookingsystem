package com.impetus.roombookingsystem.microservices.service;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.impetus.roombookingsystem.microservices.domain.RoomDomain;
import com.impetus.roombookingsystem.microservices.entity.Room;
import com.impetus.roombookingsystem.microservices.entity.RoomType;
import com.impetus.roombookingsystem.microservices.repository.RoomRepository;

@Service
public class RoomServiceImpl implements RoomService {

	@Autowired
	public RoomRepository roomRepository;

	public RoomServiceImpl() {
		super();
		System.out.println("roomRepository 111 : " + roomRepository);
	}

	@Override
	public RoomDomain findByRoomId(Integer roomId) {
		Room room = roomRepository.findByRoomId(roomId);
		return prepareAndReturnRoomDomain(room);
	}
	
	@Override
	public RoomDomain findByRoomName(String roomName) {
		Room room = roomRepository.findByRoomName(roomName);
		return prepareAndReturnRoomDomain(room);
	}

	private RoomDomain prepareAndReturnRoomDomain(Room room) {
		RoomDomain roomDomain = new RoomDomain();
		roomDomain.setRoomName(room.getRoomName());
		roomDomain.setFloor(room.getFloor());
		roomDomain.setRoomId(room.getRoomId());
		roomDomain.setPlaces(room.getPlaces());
		roomDomain.setRoomTypeId(room.getRoomType().getRoomTypeID());
		roomDomain.setRoomTypeValue(room.getRoomType().getRoomType());
		return roomDomain;
	}

	@Override
	public List<RoomDomain> getAllRooms() {

		Iterable<Room> iterator = roomRepository.findAll();
		List<Room> rooms = StreamSupport.stream(iterator.spliterator(), false).collect(Collectors.toList());

		return rooms.stream().map(room -> new RoomDomain(room.getRoomId(), room.getRoomName(), room.getFloor(),
				room.getPlaces(), room.getRoomType().getRoomTypeID(), room.getRoomType().getRoomType())).collect(Collectors.toList());
	}

	@Override
	public RoomDomain addNewRoom(RoomDomain room) throws Exception {
		Room savedRoom = roomRepository.save(mapRoomDomain(room));
		return prepareAndReturnRoomDomain(savedRoom);
	}

	private Room mapRoomDomain(RoomDomain roomDomain) throws Exception {
		Room room = new Room();
		room.setRoomName(roomDomain.getRoomName());
		room.setFloor(roomDomain.getFloor());
		room.setPlaces(roomDomain.getPlaces());

		RoomType roomType = new RoomType();
		roomType.setRoomTypeID(RoomTypeEnum.valueOf(roomDomain.getRoomTypeId()).getRoomId());
		roomType.getRooms().add(room);

		room.setRoomType(roomType);
		return room;
	}

}

enum RoomTypeEnum
{
  VC(1, "VC"), 
  MEETING_ROOM(3, "Meeting Room"), 
  AUDITORIUM(2,"Auditorium"); 
	
	private int roomId;

	private String roomType;

	private RoomTypeEnum(int value, String typeName) {
		this.roomId = value;
		this.roomType = typeName;
	}
	
	public int getRoomId() {
		return roomId;
	}

	public void setRoomId(int roomId) {
		this.roomId = roomId;
	}

	public String getRoomType() {
		return roomType;
	}

	public void setRoomType(String roomType) {
		this.roomType = roomType;
	}

	public static RoomTypeEnum valueOf(int value) throws Exception
	  {
	    switch (value)
	    {
	      case 1 :
	        return VC;
	      case 2 :
	        return MEETING_ROOM;
	      case 3 :
	        return AUDITORIUM;
	      default :
	    	  return MEETING_ROOM;
	    }
	  }
}
