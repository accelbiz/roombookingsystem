package com.impetus.roombookingsystem.microservices.repository;

import org.springframework.dao.DataAccessException;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.impetus.roombookingsystem.microservices.entity.Room;

@Repository
public interface RoomRepository extends CrudRepository<Room, Long> {
	
	Room findByRoomId(Integer roomId) throws DataAccessException;

	Room save(Room room) throws DataAccessException;
	
	Room findByRoomName(String name) throws DataAccessException;
}
