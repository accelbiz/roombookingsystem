package com.impetus.roombookingsystem.microservices.restapi;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.impetus.roombookingsystem.microservices.domain.RoomDomain;
import com.impetus.roombookingsystem.microservices.service.RoomService;

@RestController
@RequestMapping(value = "/room-service")
public class RoomResource {


	@Autowired
    public RoomService roomService;
	
	@RequestMapping(value = "/room/{id}", method = RequestMethod.GET)
	public RoomDomain getRoomById(@PathVariable Integer id) {
		return roomService.findByRoomId(id);
	}
	
	@RequestMapping(value = "/roomname/{name}", method = RequestMethod.GET)
	public RoomDomain getRoomByName(@PathVariable("name") String name) {
		return roomService.findByRoomName(name);
	}

	@RequestMapping(value = "/rooms", method = RequestMethod.GET)
	public List<RoomDomain> getAllRooms() {
		return roomService.getAllRooms();
	}

	@RequestMapping(value = "/addroom", method = RequestMethod.POST, headers = "Accept=application/json")
	public RoomDomain addRoom(@RequestBody RoomDomain room) throws Exception {
		return roomService.addNewRoom(room);
	}
}
