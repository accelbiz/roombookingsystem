package com.impetus.roombookingsystem.microservices.config;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@ComponentScan(basePackages = "com.impetus.roombookingsystem.microservices")
@EnableConfigurationProperties
@EnableTransactionManagement
@EnableJpaRepositories("com.impetus.roombookingsystem.microservices")
@EntityScan("com.impetus.roombookingsystem.microservices")
@PropertySource("classpath:/META-INF/spring/mysql.properties")
public class ApplicationConfig {
	
	
	@Autowired
	public Environment env;

	  
	  @Bean
	  public DataSource dataSource() {
	      DriverManagerDataSource dataSource = new DriverManagerDataSource();
	      dataSource.setDriverClassName(env.getRequiredProperty("database.driverClassName"));
	      dataSource.setUrl(env.getRequiredProperty("database.url"));
	      dataSource.setUsername(env.getRequiredProperty("database.username"));
	      dataSource.setPassword(env.getRequiredProperty("database.password"));
	      return dataSource;
	  }
}
