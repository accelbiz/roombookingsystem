package com.impetus.roombookingsystem.microservices;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = BookingServiceApplication.class)
@WebAppConfiguration
public class BookingServiceApplicationTests {

	@Test
	public void contextLoads() {
	// do nothing
	}

}
