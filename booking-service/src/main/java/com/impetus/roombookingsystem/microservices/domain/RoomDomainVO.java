package com.impetus.roombookingsystem.microservices.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class RoomDomainVO {

	private int roomId;

	private String roomName;

	private int floor;

	private int places;

	private int roomTypeId;
	
	private String roomTypeValue;

	public RoomDomainVO() {
		super();
	}


	public RoomDomainVO(int roomId, String roomName, int floor, int places, int roomTypeId, String roomTypeValue) {
		super();
		this.roomId = roomId;
		this.roomName = roomName;
		this.floor = floor;
		this.places = places;
		this.roomTypeId = roomTypeId;
		this.roomTypeValue = roomTypeValue;
	}


	public int getRoomId() {
		return roomId;
	}


	public void setRoomId(int roomId) {
		this.roomId = roomId;
	}


	public String getRoomName() {
		return roomName;
	}


	public void setRoomName(String roomName) {
		this.roomName = roomName;
	}


	public int getFloor() {
		return floor;
	}


	public void setFloor(int floor) {
		this.floor = floor;
	}


	public int getPlaces() {
		return places;
	}


	public void setPlaces(int places) {
		this.places = places;
	}


	public int getRoomTypeId() {
		return roomTypeId;
	}


	public void setRoomTypeId(int roomTypeId) {
		this.roomTypeId = roomTypeId;
	}


	public String getRoomTypeValue() {
		return roomTypeValue;
	}


	public void setRoomTypeValue(String roomTypeValue) {
		this.roomTypeValue = roomTypeValue;
	}


}
