package com.impetus.roombookingsystem.microservices.restapi;

import java.util.List;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.impetus.roombookingsystem.microservices.domain.RoomDomainVO;

public interface RoomResource {

	final static String PREFIX = "room-service/";
	
	@RequestMapping(value = PREFIX +"/room/{id}", method = RequestMethod.GET)
	public RoomDomainVO getRoomById(@PathVariable("id") Integer id);
	
	@RequestMapping(value = PREFIX +"/roomname/{name}", method = RequestMethod.GET)
	public RoomDomainVO getRoomByName(@PathVariable("name") String name);

	@RequestMapping(value = PREFIX +"/rooms", method = RequestMethod.GET)
	public List<RoomDomainVO> getAllRooms();

	@RequestMapping(value = PREFIX +"/addroom", method = RequestMethod.POST, headers = "Accept=application/json")
	public RoomDomainVO addRoom(@RequestBody RoomDomainVO room) throws Exception;
}
