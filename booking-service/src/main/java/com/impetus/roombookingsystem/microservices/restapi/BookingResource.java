package com.impetus.roombookingsystem.microservices.restapi;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.impetus.roombookingsystem.microservices.domain.BookingDomain;
import com.impetus.roombookingsystem.microservices.domain.RoomDomainVO;
import com.impetus.roombookingsystem.microservices.domain.UserDomainVO;
import com.impetus.roombookingsystem.microservices.service.BookingService;

@RestController
@RequestMapping(value = "/booking-service")
public class BookingResource {
	
	@Autowired
	UserResourceFeignClient userResourceFeignClient;
	
	@Autowired
	RoomResourceFeignClient roomResourceFeignClient;
	
	@Autowired
	BookingService  bookingService;
	
	@RequestMapping(value = "/addBooking", method = RequestMethod.POST, headers = "Accept=application/json")
	public BookingDomain addNewBooking(@RequestBody BookingDomain booking) {
		
		RoomDomainVO room = roomResourceFeignClient.getRoomByName(booking.getRoomName());
		booking.setRoomId(room.getRoomId());
		
		UserDomainVO user = userResourceFeignClient.getUserByEmail(booking.getUserEmail());
		booking.setUserId(user.getId());
		
		return bookingService.addBooking(booking);
	}
	
}
