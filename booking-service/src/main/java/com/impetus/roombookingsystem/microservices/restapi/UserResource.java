package com.impetus.roombookingsystem.microservices.restapi;

import java.util.List;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.impetus.roombookingsystem.microservices.domain.UserDomainVO;

public interface UserResource {

	final static String PREFIX = "user-service/";

	@RequestMapping(value = PREFIX + "/user/{id}", method = RequestMethod.GET)
	public UserDomainVO getUserById(@PathVariable("id") Integer id);

	@RequestMapping(value = PREFIX + "/userlogin/{email}", method = RequestMethod.GET)
	public UserDomainVO getUserByEmail(@PathVariable("email") String email);

	@RequestMapping(value = PREFIX + "/users", method = RequestMethod.GET)
	public List<UserDomainVO> getAllUsers();

	@RequestMapping(value = PREFIX + "/adduser", method = RequestMethod.POST, headers = "Accept=application/json")
	public UserDomainVO addUser(@RequestBody UserDomainVO user);

}
