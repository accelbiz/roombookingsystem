package com.impetus.roombookingsystem.microservices.domain;

import java.util.Date;

public class BookingDomain {

    private int bookingId;

    private int userId;
    
    private String userName;
    
    private String userEmail;
    
    private int roomId;

    private String roomName;

    private Date date;
    
    private Date starttime;
    
    private Date endtime;

	public BookingDomain() {
		super();
	}


	public BookingDomain(int bookingId, int userId, String userName, String userEmail, int roomId, String roomName,
			Date date, Date starttime, Date endtime) {
		super();
		this.bookingId = bookingId;
		this.userId = userId;
		this.userName = userName;
		this.userEmail = userEmail;
		this.roomId = roomId;
		this.roomName = roomName;
		this.date = date;
		this.starttime = starttime;
		this.endtime = endtime;
	}



	public int getBookingId() {
		return bookingId;
	}

	public void setBookingId(int bookingId) {
		this.bookingId = bookingId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserEmail() {
		return userEmail;
	}

	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}

	public String getRoomName() {
		return roomName;
	}

	public void setRoomName(String roomName) {
		this.roomName = roomName;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Date getStarttime() {
		return starttime;
	}

	public void setStarttime(Date starttime) {
		this.starttime = starttime;
	}

	public Date getEndtime() {
		return endtime;
	}

	public void setEndtime(Date endtime) {
		this.endtime = endtime;
	}



	public int getUserId() {
		return userId;
	}



	public void setUserId(int userId) {
		this.userId = userId;
	}

	public int getRoomId() {
		return roomId;
	}

	public void setRoomId(int roomId) {
		this.roomId = roomId;
	}
    
}
