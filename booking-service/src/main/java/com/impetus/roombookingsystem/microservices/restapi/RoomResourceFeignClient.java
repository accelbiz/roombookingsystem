package com.impetus.roombookingsystem.microservices.restapi;

import org.springframework.cloud.netflix.feign.FeignClient;

@FeignClient(name = "room-service")
public interface RoomResourceFeignClient extends RoomResource {
}
