package com.impetus.roombookingsystem.microservices.service;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import com.impetus.roombookingsystem.microservices.domain.BookingDomain;
import com.impetus.roombookingsystem.microservices.entity.Booking;
import com.impetus.roombookingsystem.microservices.repository.BookingRepository;

@Service
public class BookingServiceImpl implements BookingService {

	@Autowired
	public BookingRepository bookingRepository;

	public BookingServiceImpl() {
		super();
		System.out.println("bookingRepository 111 : " + bookingRepository);
	}

	public BookingServiceImpl(BookingRepository bookingRepository) {
		super();
		this.bookingRepository = bookingRepository;
	}

	@Override
	public List<BookingDomain> getAllBookingsByRoomId(Integer roomId) {
		List<Booking> bookingsByRoomId = bookingRepository.getAllBookingsByRoomId(roomId);
		return bookingsByRoomId.stream().map(booking -> prepareBookingDomain(booking)).collect(Collectors.toList());
	}

	@Override
	public BookingDomain addBooking(BookingDomain booking) throws DataAccessException {
		Booking savedBooking = bookingRepository.save(mapBookingDomain(booking));
		return prepareBookingDomain(savedBooking);
	}

	private Booking mapBookingDomain(BookingDomain bookingDomain) {
		Booking booking = new Booking();
		booking.setDate(convertUtilToSql(bookingDomain.getDate()));
		booking.setRoomId(bookingDomain.getRoomId());
		booking.setUserId(bookingDomain.getUserId());
		booking.setEndtime(convertUtilToSql(bookingDomain.getEndtime()));
		booking.setStarttime(convertUtilToSql(bookingDomain.getStarttime()));
		return booking;
	}

	private BookingDomain prepareBookingDomain(Booking booking) {
		return new BookingDomain(booking.getBookingId(), booking.getUserId(), null, null, booking.getRoomId(), null,
				booking.getDate(), booking.getStarttime(), booking.getEndtime());
	}

	private static java.sql.Timestamp convertUtilToSql(Date uDate) {
		java.sql.Timestamp sDate = new java.sql.Timestamp(uDate.getTime());
		return sDate;
	}

}
