package com.impetus.roombookingsystem.microservices.repository;

import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.impetus.roombookingsystem.microservices.entity.Booking;

@Repository
public interface BookingRepository extends CrudRepository<Booking, Long> {
	
	List<Booking> getAllBookingsByRoomId(Integer id);
	
	Booking save(Booking booking) throws DataAccessException;
	
}
