package com.impetus.roombookingsystem.microservices.entity;

import java.sql.Date;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "booking")
public class Booking {

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "booking_id")
    private int bookingId;

	@Column(name = "user_id")
    private int userId;

	@Column(name = "room_id")
    private int roomId;

	@Column(name = "time_slot_id")
    private int timeSlotId;

	@Column(name = "date")
    private Timestamp date;
    
	@Column(name = "starttime")
    private Timestamp starttime;
    
	@Column(name = "endtime")
    private Timestamp endtime;


	public int getBookingId() {
		return bookingId;
	}


	public void setBookingId(int bookingId) {
		this.bookingId = bookingId;
	}


	public int getUserId() {
		return userId;
	}


	public void setUserId(int userId) {
		this.userId = userId;
	}


	public int getRoomId() {
		return roomId;
	}


	public void setRoomId(int roomId) {
		this.roomId = roomId;
	}


	public int getTimeSlotId() {
		return timeSlotId;
	}


	public void setTimeSlotId(int timeSlotId) {
		this.timeSlotId = timeSlotId;
	}


	public Timestamp getDate() {
		return date;
	}


	public void setDate(Timestamp date) {
		this.date = date;
	}


	public Timestamp getStarttime() {
		return starttime;
	}


	public void setStarttime(Timestamp starttime) {
		this.starttime = starttime;
	}


	public Timestamp getEndtime() {
		return endtime;
	}


	public void setEndtime(Timestamp endtime) {
		this.endtime = endtime;
	}


	@Override
	public String toString() {
		return "Booking [bookingId=" + bookingId + ", userId=" + userId + ", roomId=" + roomId + ", timeSlotId="
				+ timeSlotId + ", date=" + date + ", starttime=" + starttime + ", endtime=" + endtime + "]";
	}
}
