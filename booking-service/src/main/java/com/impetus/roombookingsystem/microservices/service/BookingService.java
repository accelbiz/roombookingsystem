package com.impetus.roombookingsystem.microservices.service;

import java.util.List;

import org.springframework.dao.DataAccessException;

import com.impetus.roombookingsystem.microservices.domain.BookingDomain;

public interface BookingService {

	List<BookingDomain> getAllBookingsByRoomId(Integer id);

	BookingDomain addBooking(BookingDomain booking) throws DataAccessException;

}