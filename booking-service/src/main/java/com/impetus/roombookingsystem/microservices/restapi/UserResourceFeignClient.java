package com.impetus.roombookingsystem.microservices.restapi;

import org.springframework.cloud.netflix.feign.FeignClient;

@FeignClient(name = "user-registeration-service")
public interface UserResourceFeignClient extends UserResource {
	
}
