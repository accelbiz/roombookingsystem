package com.impetus.roombookingsystem.microservices.service;

import java.util.List;

import com.impetus.roombookingsystem.microservices.domain.UserDomain;

public interface UserService {

	UserDomain getUserById(Integer id);
	
	UserDomain getUserByEmail(String email);

	List<UserDomain> getAllUsers();

	UserDomain addNewUser(UserDomain user);

}