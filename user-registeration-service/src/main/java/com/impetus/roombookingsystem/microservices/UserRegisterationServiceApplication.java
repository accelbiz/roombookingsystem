package com.impetus.roombookingsystem.microservices;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class UserRegisterationServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(UserRegisterationServiceApplication.class, args);
	}
}
