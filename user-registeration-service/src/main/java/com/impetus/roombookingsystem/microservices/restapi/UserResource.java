package com.impetus.roombookingsystem.microservices.restapi;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.impetus.roombookingsystem.microservices.domain.UserDomain;
import com.impetus.roombookingsystem.microservices.service.UserService;

@RestController
@RequestMapping(value = "/user-service")
public class UserResource {

	@Autowired
	public UserService userService;

	@RequestMapping(value = "/user/{id}", method = RequestMethod.GET)
	public UserDomain getUserById(@PathVariable Integer id) {
		return userService.getUserById(id);
	}
	
	@RequestMapping(value = "/userlogin/{email:.+}", method = RequestMethod.GET)
	public UserDomain getUserByEmail(@PathVariable("email") String email) {
		return userService.getUserByEmail(email);
	}

	@RequestMapping(value = "/users", method = RequestMethod.GET)
	public List<UserDomain> getAllUsers() {
		return userService.getAllUsers();
	}

	@RequestMapping(value = "/adduser", method = RequestMethod.POST, headers = "Accept=application/json")
	public UserDomain addUser(@RequestBody UserDomain user) {
		return userService.addNewUser(user);
	}

}
