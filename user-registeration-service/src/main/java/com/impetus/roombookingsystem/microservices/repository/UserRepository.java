package com.impetus.roombookingsystem.microservices.repository;

import org.springframework.dao.DataAccessException;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.impetus.roombookingsystem.microservices.entity.User;

@Repository
public interface UserRepository extends CrudRepository<User, Long> {
	
	User findById(Integer id) throws DataAccessException;

	User save(User user) throws DataAccessException;
	
	User findByEmail(String email) throws DataAccessException;
}
