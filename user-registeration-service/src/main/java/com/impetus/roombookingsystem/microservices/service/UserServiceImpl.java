package com.impetus.roombookingsystem.microservices.service;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.impetus.roombookingsystem.microservices.domain.UserDomain;
import com.impetus.roombookingsystem.microservices.entity.User;
import com.impetus.roombookingsystem.microservices.repository.UserRepository;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	public UserRepository userRepository;

	public UserServiceImpl() {
		super();
		System.out.println("userRepository 111 : " + userRepository);
	}

	public UserServiceImpl(UserRepository userRepository) {
		super();
		this.userRepository = userRepository;
		System.out.println("userRepository : " + userRepository);
		User findOne = userRepository.findOne(1L);
		System.out.println("Value of findone " + findOne);
	}

	@Override
	public UserDomain getUserById(Integer id) {
		User user = userRepository.findById(id);
		return prepareAndReturnUserDomain(user);
	}
	
	@Override
	public UserDomain getUserByEmail(String email) {
		User user = userRepository.findByEmail(email);
		return prepareAndReturnUserDomain(user);
	}

	@Override
	public List<UserDomain> getAllUsers() {

		Iterable<User> iterator = userRepository.findAll();
		List<User> users = StreamSupport.stream(iterator.spliterator(), false).collect(Collectors.toList());
		return users.stream().map(user -> prepareAndReturnUserDomain(user)).collect(Collectors.toList());
	}

	@Override
	public UserDomain addNewUser(UserDomain user) {
		User savedUser = userRepository.save(mapUserDomain(user));
		return prepareAndReturnUserDomain(savedUser);
	}

	private UserDomain prepareAndReturnUserDomain(User user) {
		return new UserDomain(user.getId(), user.getPassword(), user.getEmail(), user.getFirstName(),
				user.getLastName(), user.getRole(), user.getDesignation());
	}

	private User mapUserDomain(UserDomain userDomain) {
		User user = new User();
		user.setDesignation(userDomain.getDesignation());
		user.setEmail(userDomain.getEmail());
		user.setFirstName(userDomain.getFirstName());
		user.setLastName(userDomain.getLastName());
		user.setPassword(userDomain.getPassword());
		user.setRole(userDomain.getRole());
		return user;
	}

}
