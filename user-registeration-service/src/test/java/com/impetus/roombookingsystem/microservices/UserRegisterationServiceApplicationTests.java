package com.impetus.roombookingsystem.microservices;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.impetus.roombookingsystem.microservices.entity.User;
import com.impetus.roombookingsystem.microservices.repository.UserRepository;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = UserRegisterationServiceApplication.class)
public class UserRegisterationServiceApplicationTests {

	@Autowired
	UserRepository  userRepository;
	@Test
	 public void testLoadGames() {
	     List<User> games = (ArrayList<User>) userRepository.findAll();
	     assertEquals("Did not get all games", 3, games.size());
	 }

}
